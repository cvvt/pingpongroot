
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := debugexploit
LOCAL_SRC_FILES := main.c getroot.c
LOCAL_LDFLAGS   += -llog -pie -fPIE
LOCAL_CFLAGS    += -DDEBUG -DPORT=12345
LOCAL_CFLAGS    += -fno-stack-protector -O0 -fPIE
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_CFLAGS    += -fno-stack-protector -O0
LOCAL_MODULE    := exploit
LOCAL_CFLAGS	+= -DPORT=12345
LOCAL_SRC_FILES := main.c getroot.c

include $(BUILD_SHARED_LIBRARY)


